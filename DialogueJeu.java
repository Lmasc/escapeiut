import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;


public class DialogueJeu extends Dialogue {

    public void intro() {

        p.setPV(p.obtenirPVDepart());
        System.out.println(p.getPV());
        String dialogue_jeu = "./textes/intro_dial_jeu.txt";
        System.out.println(Read.readLineByLine(dialogue_jeu));
        (new DialogueTenib()).AppelTenib(p.getPV());
      }

    //public void donnerPV() {

  //  }

    //public void PremierCouloir() {

        //(new DialoguePerso()).choisirSalle();
    //}


    // public void Salle_Informatique() {
    // }

    // public void Bureau_Prof() {
    // }

    // public void Salle105() {
    // }

    // public void Toilettes() {
    // }

    // public void Inventaire() {
    // }

    public void introFuiteFinale(){
      String dialogue_fuite = "./textes/intro_dial_fuite.txt";
      System.out.println(Read.readLineByLine(dialogue_fuite));
      System.out.println("Vous pouvez faire jusqu’à 6 points de dégâts et vous possédez actuellement " +p.getPV()+ "\n");
      String dialogue_fuite_ask = "./textes/intro_dial_fuite_ask.txt";
      System.out.println(Read.readLineByLine(dialogue_fuite_ask));
        (new DialoguePerso()).choisirFuiteFinale();
    }

    public void fautVraimentEtreBete(){
      String dingue = "./textes/dingue.txt";
      System.out.println(Read.readLineByLine(dingue));
    }
}
