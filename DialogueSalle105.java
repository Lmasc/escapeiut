import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueSalle105 extends Dialogue {

   public void Intro_Salle105(int PV) {
      p.setPV(PV);
      String intro = "./textes/dial105/dial105_intro.txt";
      System.out.println(Read.readLineByLine(intro));
      choixPorte(p.getPV());
    }

   public void choixPorte(int PV){
      p.setPV(PV);
      String choix = "./textes/dial105/dial105_ask.txt";
      System.out.println(Read.readLineByLine(choix));
      Scanner sc = new Scanner(System.in);
      int choixporte = sc.nextInt();

        switch(choixporte){
          case 1 :
          String filePath1 = "./textes/dial105/dial105_ask_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));
          choixCommencer(p.getPV());
          break;
          case 2 :
          String filePath2 = "./textes/dial105/dial105_ask_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          choixPorte(p.getPV());
          break;
          case 3 :
          String filePath3 = "./textes/dial105/dial105_ask_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          choixPorte(p.getPV());
          break;
          case 4 :
          String filePath4 = "./textes/dial105/dial105_ask_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          choixPorte(p.getPV());
          break;
          case 5 :
          String filePath5 = "./textes/dial105/dial105_ask_rep5.txt";
          System.out.println(Read.readLineByLine(filePath5));
          choixPorte(p.getPV());
          break;
          case 6 :
          String filePath6 = "./textes/dial105/dial105_ask_rep6.txt";
          System.out.println(Read.readLineByLine(filePath6));
          choixPorte(p.getPV());
          break;
          case 7 :
          String filePath7 = "./textes/dial105/dial105_ask_rep7.txt";
          System.out.println(Read.readLineByLine(filePath7));
          baisserPV();
              if (p.getPV() <= 0)
              {
                (new Mort()).GameOver();
                String s = sc.nextLine();
                if(s.equals("oui")){
                  Intro_Salle105(5);
                } else {
              //    (new Mort()).adieu();
                }
              }
          System.out.println("Vous prenez les feuilles et vous les remettez sur la table.");
          choixPorte(p.getPV());
          break;
          default :
          System.out.println("Choix non reconnu");
          choixPorte(p.getPV());
          break;
        }
   }

   public void choixCommencer(int PV){
     p.setPV(PV);
     String filePath1 = "./textes/dial105/dial105_ask_rep1_choix.txt";
     System.out.println(Read.readLineByLine(filePath1));
     Scanner sc = new Scanner(System.in);
     int choix = sc.nextInt();
     switch(choix){
       case 1 :
        DeplacerTable(p.getPV());
        break;
       case 2 :
        choixCommencer(p.getPV());
        break;
       default :
        choixCommencer(p.getPV());
        break;
   }
 }

   public void DeplacerTable(int PV){
     p.setPV(PV);
     String filePath1 = "./textes/dial105/dial105_deplacer_table.txt";
     System.out.println(Read.readLineByLine(filePath1));
     Scanner sc = new Scanner(System.in);
     int choix = sc.nextInt();
     switch(choix){
       case 1 :
        System.out.println("VOUS : Bah non ");
        break;
       case 2 :
       System.out.println("VOUS : Euh oui, oui, bien sûr… ");
        break;
       case 3 :
        System.out.println("VOUS : Ça arrive souvent ce genre de phénomène ? ");
        System.out.println("Deuxième élève : A chaque fois qu’on déplace une table.");
       default :
        DeplacerTable(p.getPV());
        break;
   }
   String filePath2 = "./textes/dial105/dial105_fin_table.txt";
   System.out.println(Read.readLineByLine(filePath2));
   String filePath3 = "./textes/dial105/dial105_ask2_intro.txt";
   System.out.println(Read.readLineByLine(filePath3));
   choixPhilo(p.getPV());
 }

   public void choixPhilo(int PV){
      p.setPV(PV);
      String ask2 = "./textes/dial105/dial105_ask2.txt";
      System.out.println(Read.readLineByLine(ask2));
      Scanner sc = new Scanner(System.in);
      int choixphilo = sc.nextInt();

      switch(choixphilo){
          case 1 :
          String filePath1 = "./textes/dial105/dial105_ask2_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));
          choixPhilo(p.getPV());
          break;
          case 2 :
          String filePath2 = "./textes/dial105/dial105_ask2_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          choixPhilo(p.getPV());
          break;
          case 3 :
          String filePath3 = "./textes/dial105/dial105_ask2_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          String filePath5 = "./textes/dial105/dial105_ask2_rep4.txt";
          System.out.println(Read.readLineByLine(filePath5));
          choixPapier(p.getPV());
          break;
          case 4 :
          String filePath4 = "./textes/dial105/dial105_ask2_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          choixPapier(p.getPV());
          break;
          default:
          System.out.println("Choix non reconnu");
          choixPhilo(p.getPV());
          break;
        }
    }

    public void choixPapier(int PV){
      p.setPV(PV);
       String filePath = "./textes/dial105/dial105_ask3.txt";
       System.out.println(Read.readLineByLine(filePath));
       Scanner sc = new Scanner(System.in);
       int choixpapier = sc.nextInt();

      switch(choixpapier){
          case 1 :
          String filePath1 = "./textes/dial105/dial105_ask3_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));
          Intro_Salle105(p.getPV());
          break;
          case 2 :
          String filePath2 = "./textes/dial105/dial105_ask3_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          (new DialogueCouloir()).choisirSalle(p.getPV());
          break;
          case 3 :
          String filePath3 = "./textes/dial105/dial105_ask3_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          (new DialogueCouloir()).choisirSalle(p.getPV());
          break;
          case 4 :
          String filePath4 = "./textes/dial105/dial105_ask3_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          (new Inventaire()).Liste_inventaire(4);
          (new DialogueCouloir()).choisirSalle(p.getPV());
          break;
          default :
          System.out.println("Choix non reconnu");
          choixPapier(p.getPV());
          break;
        }
    }

    public void baisserPV(){
        Random random = new Random();
        int degats = random.nextInt(6);
        p.setPV(p.getPV()-degats);
        System.out.println("Vous avez pris "+degats+" point(s) de degats, il vous reste " + p.getPV() + "/" + p.getMaxPV() + " PV.");
    }
}
