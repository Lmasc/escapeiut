

private class PersoSingleton
{
    private static PersoSingleton instance = new PersoSingleton();
    private String nom;
    private int pv;
    private int maxPv;
    private Inventaire inv;

    private PersoSingleton() {}

    private static PersoSingleton getInstance()
    {
        return (instance);
    }

    public void setAttributes()
    {
        this.nom = nom;
        this.maxPv = PointsVie.PV;
        this.pv = this.maxPv;
        inv = new Inventaire();
    }

    public String toString() {
        return ("nom = " + nom + ", pv actuels = " + pv + ", pv max = "+ maxPv);
    }

    public String getNom()
    {
        return (nom);
    }

    public String getPv()
    {
        return (pv);
    }
    public String getMaxPv()
    {
        return (maxPv);
    }

    public void setNom(String s){
        this.nom = s;
     }

     public void setPV(int nb){
        this.pv = nb;
     }

     public void setMaxPV(int nb){
        if(nb>this.pv)
            this.MaxPV = nb;
        else
            this.MaxPV = this.pv;
    }
}
