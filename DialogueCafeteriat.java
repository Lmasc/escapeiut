import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueCafeteriat extends Dialogue {


  public void Cafeteria(int PV)
  {
     p.setPV(PV);
     String intro = "./textes/dial_cafet/dial_cafet.txt";
     System.out.println(Read.readLineByLine(intro));
     choixCornelien(p.getPV());
  }

  public void choixCornelien(int PV)
  {
     p.setPV(PV);
     String choixcornel = "./textes/dial_cafet/dial_cafet_ask.txt";
     System.out.println(Read.readLineByLine(choixcornel));
     Scanner sc = new Scanner(System.in);
     int choix_cafet = sc.nextInt();

     switch(choix_cafet){
          case 1 :
          String filePath1 = "./textes/dial_cafet/dial_rep1.txt";
          System.out.println(Read.readLineByLine(filePath1));
          (new DialogueEgairam()).Intro_Egairam("cafe", p.getPV());
          break;
          case 2 :
          String filePath2 = "./textes/dial_cafet/dial_rep2.txt";
          System.out.println(Read.readLineByLine(filePath2));
          (new DialogueEgairam()).Intro_Egairam("nourriture", p.getPV());
          break;
          case 3 :
          String filePath3 = "./textes/dial_cafet/dial_rep3.txt";
          System.out.println(Read.readLineByLine(filePath3));
          (new DialogueEgairam()).Intro_Egairam("boisson", p.getPV());
          break;
          case 4 :
          String filePath4 = "./textes/dial_cafet/dial_rep4.txt";
          System.out.println(Read.readLineByLine(filePath4));
          (new DialogueJardin()).choixJardin(p.getPV());
          break;
          case 5 :
          String filePath5 = "./textes/dial_cafet/dial_rep5.txt";
          System.out.println(Read.readLineByLine(filePath5));
          choixCornelien(p.getPV());
          break;
          case 6 :
          String filePath6 = "./textes/dial_cafet/dial_destruction.txt";
          System.out.println(Read.readLineByLine(filePath6));
          (new DialogueEgairam()).Intro_Egairam("",p.getPV());
          break;
          default :
          System.out.println("Choix non reconnu");
          choixCornelien(p.getPV());
          break;
     }
  }



}
