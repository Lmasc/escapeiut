import java.util.Scanner;

public class DialogueSecretariat extends Dialogue {
    protected Scanner sc = new Scanner(System.in);

    public void Intro_Secretariat(int PV) {
        p.setPV(PV);
        String intro = "./textes/dial_secretariat/secretariat_intro.txt";
        System.out.println(Read.readLineByLine(intro));
        choixRep(p.getPV());
    }

    public void choixRep(int PV){
        p.setPV(PV);
        String choix = "./textes/dial_secretariat/dial_secretariat_ask.txt";
        System.out.println(Read.readLineByLine(choix));
        int choixrep = sc.nextInt();

        switch(choixrep){
            case 1 :
            String filePath1 = "./textes/dial_secretariat/dial_secretariat_rep_1.txt";
            System.out.println(Read.readLineByLine(filePath1));
            choixCombat(p.getPV());
            break;
            case 2 :
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_rep_2.txt";
            System.out.println(Read.readLineByLine(filePath2));
            choixCombat(p.getPV());
            break;
            case 3 :
            String filePath3 = "./textes/dial_secretariat/dial_secretariat_rep_3.txt";
            System.out.println(Read.readLineByLine(filePath3));
            choix3(p.getPV());
            break;
            case 4 :
            String filePath4 = "./textes/dial_secretariat/dial_secretariat_rep_4.txt";
            System.out.println(Read.readLineByLine(filePath4));
            choix4(p.getPV());
            break;
        }
    }

    public void choix3(int PV){
        p.setPV(PV);
        int choixrep = sc.nextInt();
        switch(choixrep){
            case 1 :
            String filePath1 = "./textes/dial_secretariat/dial_secretariat_rep_3_rep1.txt";
            System.out.println(Read.readLineByLine(filePath1));
            choixCombat(p.getPV());
            break;
            case 2 :
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_rep_3_rep2.txt";
            System.out.println(Read.readLineByLine(filePath2));
            choixCombat(p.getPV());
            break;
            case 3 :
            String filePath3 = "./textes/dial_secretariat/dial_secretariat_rep_3_rep3.txt";
            System.out.println(Read.readLineByLine(filePath3));
            choixCombat(p.getPV());
            break;
        }
    }

    public void choix4(int PV){
        p.setPV(PV);
        int choixrep = sc.nextInt();
        switch(choixrep){
            case 1 :
            String filePath1 = "./textes/dial_secretariat/dial_secretariat_rep_4_rep1.txt";
            System.out.println(Read.readLineByLine(filePath1));
            choixCombat(p.getPV());
            break;
            case 2 :
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_rep_4_rep2.txt";
            System.out.println(Read.readLineByLine(filePath2));
            choixCombat(p.getPV());
            break;
            case 3 :
            String filePath3 = "./textes/dial_secretariat/dial_secretariat_rep_4_rep3.txt";
            System.out.println(Read.readLineByLine(filePath3));
            choixCombat(p.getPV());
            break;
        }
    }

    public void choixCombat(int PV){
        p.setPV(PV);
        String choix = "./textes/dial_secretariat/dial_secretariat_rep_combat.txt";
        System.out.println(Read.readLineByLine(choix));
        int choixrep = sc.nextInt();
        switch(choixrep){
            case 1 :
            (new Combat(p.getPV())).Combat();
            String filePath1 = "./textes/dial_secretariat/dial_secretariat_victoire.txt";
            System.out.println(Read.readLineByLine(filePath1));
            choixVictoire(p.getPV());
            break;
            case 2 :
            (new Combat(p.getPV())).lancerRouleau();
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_victoire.txt";
            System.out.println(Read.readLineByLine(filePath2));
            choixVictoire(p.getPV());
            break;
            case 3 :
            String filePath3 = "./textes/dial_secretariat/dial_secretariat_rep_combat_rep3.txt";
            System.out.println(Read.readLineByLine(filePath3));
            (new DialogueCouloir()).choisirSalle(p.getPV());
            break;
        }
    }

    public void choixVictoire(int PV){
        p.setPV(PV);
        int choixrep = sc.nextInt();
        switch(choixrep){
            case 1 :
            String filePath1 = "./textes/dial_secretariat/dial_secretariat_chercher_justif.txt";
            System.out.println(Read.readLineByLine(filePath1));
            choixAvecJustif(p.getPV());
            break;
            case 2 :
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_partir_cafet.txt";
            System.out.println(Read.readLineByLine(filePath2));
            (new DialogueCafeteriat()).choixCornelien(p.getPV());
            break;
        }        
    }

    public void choixAvecJustif(int PV){
        p.setPV(PV);
        int choixrep = sc.nextInt();
        switch(choixrep){
            case 1 :
            (new DialogueBureauIUT()).Intro_Bureau_IUT();
            break;
            case 2 :
            String filePath2 = "./textes/dial_secretariat/dial_secretariat_partir_cafet.txt";
            System.out.println(Read.readLineByLine(filePath2));
            (new DialogueCafeteriat()).choixCornelien(p.getPV());
            break;
        }        

    }

}
