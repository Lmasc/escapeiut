import java.util.Scanner;

public class DialoguePerso extends Dialogue{

    public int choisirDifficulte(){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            p.setPV(25);
            break;
            case 2:
            p.setPV(15);
            break;
            case 3:
            p.setPV(10);
            break;
            case 4:
            p.setPV(5);
            break;
            default:
            System.out.println("erreur. L'univers t'accorde 15 pts.");
            p.setPV(15);
            p.setMaxPV(25);
            break;
        }
        return p.getPV();
    }

    // public static void choisirSalle(){
        // int choix = sc.nextInt();
        // switch (choix) {
            // case 1:
            // dj.Salle_Informatique();
            // break;
            // case 2:
            // dj.Bureau_Prof();
            // break;
            // case 3:
            // dj.Salle105();
            // break;
            // case 4:
            // dj.Toilettes();
            // break;
            // case 5:
            // dj.Inventaire();
            // break;
            // case 6:
            // (new Plan(1,1)).afficherPlan();
            // dj.PremierCouloir();
            // break;
            // default:
            // System.out.println("Choix non reconnu");
            // dj.PremierCouloir();
            // break;
        // }
    // }

    public void choisirFuiteFinale(){
        Scanner sc=new Scanner(System.in);
        int choix = sc.nextInt();
        switch (choix) {
            case 1:
            (new DialogueJeu()).fautVraimentEtreBete();
            (new DialogueJeu()).introFuiteFinale();
            break;
            case 2:
            System.out.println("La fuite peut commencer.");
            break;
        }
    }
}
