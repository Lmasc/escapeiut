import java.util.Scanner;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DialogueBibliotheque extends Dialogue {

  public void Intro_Bibliotheque_IUT() {
    String Intro_Bibliotheque_IUT = "./textes/dial_biblio/intro.txt";
    System.out.println(Read.readLineByLine(Intro_Bibliotheque_IUT));
    Scanner sc = new Scanner(System.in);
    int choix = sc.nextInt();
      switch (choix) {
        case 1:
          String biblio1 = "./textes/dial_biblio/intro.txt";
          System.out.println(Read.readLineByLine(biblio1));
          (new DialoguePetitCouloir()).Petit_Couloir(false);
          break;
        case 2:

          (new Inventaire()).Liste_inventaire(3);
          (new DialoguePetitCouloir()).Petit_Couloir(true);
          break;
        default:
          System.out.println("Choix non reconnu");
          Intro_Bibliotheque_IUT();
          break;

        }
      }

  /*  public static void main (String[]args){
      (new DialogueBibliotheque()).Intro_Bibliotheque_IUT();
    } */



}
